package main

import (
	"encoding/json"
	"fmt"
	"log"

	"example.com/dag/dag"
)

type JsonItem struct {
	ID    int   `json:"id"`
	Value int   `json:"value"`
	Links []int `json:"links"`
}

const data = `
	[
		{"id": 1, "value": 2, "links": [2, 3, 4]},
		{"id": 2, "value": 3, "links": [5, 7]},
		{"id": 3, "value": 3, "links": [5]},
		{"id": 4, "value": 4, "links": [8]},
		{"id": 5, "value": 2, "links": [6]},
		{"id": 8, "value": 2, "links": []},
		{"id": 6, "value": 2, "links": []},
		{"id": 7, "value": 2, "links": []}
	]
`

func loadGraphFromJson(data []byte) (*dag.Graph, error) {
	var items []*JsonItem
	if err := json.Unmarshal(data, &items); err != nil {
		return nil, err
	}

	ret := dag.NewGraph()

	for _, item := range items {
		if _, err := ret.AddNode(item.ID, item.Value); err != nil {
			return nil, err
		}
	}

	for _, item := range items {
		for _, linkID := range item.Links {
			if err := ret.AddEdge(item.ID, linkID); err != nil {
				return nil, err
			}
		}
	}

	return ret, nil
}

// printGraph распечатывает граф в топологическом порядке.
//
// 1: val=2, sum=2 => [2 3 4]
//    2: val=3, sum=5 => [5 7]
//       7: val=2, sum=7 => []
//    3: val=3, sum=5 => [5]
//       5: val=2, sum=10 => [6]
//          6: val=2, sum=12 => []
//    4: val=4, sum=6 => [8]
//       8: val=2, sum=8 => []
//
func printGraph(graph *dag.Graph) {
	var indent string

	graph.TSortEach(func(node *dag.Node, level int) {
		indent = ""
		for i := 0; i < level; i++ {
			indent += "   "
		}

		fmt.Printf(
			"%s%d: val=%d, sum=%d => %v\n",
			indent,
			node.ID,
			node.Value,
			node.Sum,
			node.ChildrenIDs(),
		)
	})
}

func main() {
	graph, err := loadGraphFromJson([]byte(data))
	if err != nil {
		log.Fatal(err)
	}

	graph.CalculateSums()

	printGraph(graph)
}
