package dag

import (
	"errors"
	"fmt"
)

type Node struct {
	ID       int
	Value    int
	Sum      int
	children map[int]*Node
	parents  map[int]*Node
}

// ChildrenIDs возвращает id дочерних узлов.
func (n *Node) ChildrenIDs() []int {
	ret := make([]int, len(n.children))

	var i int
	for childID := range n.children {
		ret[i] = childID
		i++
	}

	return ret
}

// childrenNodes метод-хелпер, возвращает список дочерних узлов.
func (n *Node) childrenNodes() []*Node {
	ret := make([]*Node, len(n.children))

	var i int
	for _, child := range n.children {
		ret[i] = child
		i++
	}

	return ret
}

// parentNodes метод-хелпер, возвращает список родительских улов.
func (n *Node) parentNodes() []*Node {
	ret := make([]*Node, len(n.parents))

	var i int
	for _, parent := range n.parents {
		ret[i] = parent
		i++
	}

	return ret
}

// eachParent итерирует уникальные родительские вершины в DFS-порядке.
func (n *Node) eachParent(callback func(*Node)) {
	visitedSet := make(map[*Node]bool)
	stack := newNodeStack()

	pushNodes(&stack, n.parentNodes()...)

	for node := popNode(&stack); node != nil; node = popNode(&stack) {
		if visitedSet[node] {
			continue
		}

		callback(node)
		visitedSet[node] = true

		if len(node.parents) > 0 {
			pushNodes(&stack, node.parentNodes()...)
		}
	}
}

// addChild ассоциирует с указанной вершиной дочрнюю, одновременно
// проставляя родительскую связь.
func (n *Node) addChild(child *Node) {
	if child.parents == nil {
		child.parents = make(map[int]*Node)
	}
	child.parents[n.ID] = n

	if n.children == nil {
		n.children = make(map[int]*Node)
	}
	n.children[child.ID] = child
}

// ----------------------------------------------------------------------------

// Graph имплементация графа. Вершины и ребра хранятся как списки смежности.
type Graph struct {
	nodes map[int]*Node
	roots map[int]*Node
}

func NewGraph() *Graph {
	return &Graph{
		nodes: make(map[int]*Node),
		roots: make(map[int]*Node),
	}
}

// AddNode добавляет вершину в граф.
func (g *Graph) AddNode(id int, value int) (*Node, error) {
	if node, ok := g.nodes[id]; ok {
		return node, fmt.Errorf("node id=%d already esists.", id)
	}

	g.nodes[id] = &Node{ID: id, Value: value}

	return g.nodes[id], nil
}

// AddEdge добвляет ребро графа между указанным вершинами.
func (g *Graph) AddEdge(fromID, toID int) error {
	src, dst := g.nodes[fromID], g.nodes[toID]

	if src == nil || dst == nil {
		return errors.New("can't add edge %d --> %d: vertex doesn't exists")
	}

	src.addChild(dst)

	if len(src.parents) == 0 {
		g.roots[src.ID] = src
	} else {
		delete(g.roots, src.ID)
	}

	return nil
}

// GetNodes возвращает неупорядоченный список узлов.
func (g *Graph) GetNodes() []*Node {
	ret := make([]*Node, 0, len(g.nodes))

	for _, node := range g.nodes {
		ret = append(ret, node)
	}

	return ret
}

// GetNode возвращает узел по id.
func (g *Graph) GetNode(id int) *Node {
	return g.nodes[id]
}

// CalculateSums заполняет суммы для всего графа, делая DFS-обход.
func (g *Graph) CalculateSums() {
	metaRoot := &Node{children: g.roots}
	stack := newNodeStack()
	visitedSet := make(map[*Node]bool)

	pushNodes(&stack, metaRoot.childrenNodes()...)

	for node := popNode(&stack); node != nil; node = popNode(&stack) {
		if visitedSet[node] {
			continue
		}

		// Для вершин с одним родителем (или без) просто плюсуем
		// ее собственное значение.
		// Для вершин с неск. родителями делаем обход всех родительских вершин
		// и считаем их сумму.
		if len(node.parents) == 0 {
			node.Sum = node.Value
		} else if len(node.parents) == 1 {
			node.Sum = node.parentNodes()[0].Sum + node.Value
		} else {
			sum := 0
			node.eachParent(func(parent *Node) {
				sum += parent.Value
			})

			node.Sum = sum + node.Value
		}

		visitedSet[node] = true

		if len(node.children) > 0 {
			pushNodes(&stack, node.childrenNodes()...)
		}
	}
}

// TSortEach - итерирует вершины графа отсортированные в топологическом порядке
// (т.е. в порядке зависимости).
func (g *Graph) TSortEach(callback func(n *Node, level int)) {
	metaRoot := &Node{children: g.roots}
	stack := newNodeStack()
	curPathSet := make(map[*Node]bool)
	visitedSet := make(map[*Node]bool)

	type tData struct {
		node  *Node
		level int
	}
	results := make([]*tData, 0, len(g.nodes))

	levelCounter := 0
	pushNodes(&stack, metaRoot.childrenNodes()...)
	for node := peekNode(stack); node != nil; node = peekNode(stack) {
		// Если уже обходили вершину, то пропускам ее.
		// (Это возможно так как вершины могут иметь неск. потомков.)
		if visitedSet[node] {
			popNode(&stack)

			continue
		}

		// Если вершина есть в текущем пути, значит сейчас конец ее обхода.
		if curPathSet[node] {
			delete(curPathSet, node)
			visitedSet[node] = true
			popNode(&stack)

			levelCounter -= 1
			results = append(results, &tData{node: node, level: levelCounter})

			continue
		}

		// Если вершина без потомков, значит засчитываем ее обход.
		if len(node.children) == 0 {
			visitedSet[node] = true
			popNode(&stack)

			results = append(results, &tData{node: node, level: levelCounter})

			continue
		}

		// Если есть потомки, то продолжаем DFS-обход, дополняя путь и увеливая счетчик уровня.
		curPathSet[node] = true
		levelCounter += 1
		pushNodes(&stack, node.childrenNodes()...)
	}

	// Топологическая сортировка получилась в обратном порядке, переворачиваем...
	for i := len(results) - 1; i >= 0; i-- {
		callback(results[i].node, results[i].level)
	}
}
