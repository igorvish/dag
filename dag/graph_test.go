package dag

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGraph_AddNode(t *testing.T) {
	graph := NewGraph()

	n1, err := graph.AddNode(1, 999)
	require.NoError(t, err)
	require.NotNil(t, n1, "It should create new node.")
	assert.Equal(t, 999, n1.Value, "It should assign proper values.")

	n2, err := graph.AddNode(n1.ID, n1.Value)
	require.Error(t, err, "It should return error if node already added.")
	assert.Equal(t, n1, n2, "It should not create new node.")
}

func TestGraph_AddEdge(t *testing.T) {
	graph := NewGraph()

	n1, _ := graph.AddNode(1, 999)
	n2, _ := graph.AddNode(2, 999)

	t.Run("SuccessfulCase", func(t *testing.T) {
		err := graph.AddEdge(n1.ID, n2.ID)
		require.NoError(t, err, "It should add edge witout errors")

		assert.Contains(t, n1.ChildrenIDs(), n2.ID, "It should add specified node as child.")
	})

	t.Run("When specified node doesn't esists", func(t *testing.T) {
		err := graph.AddEdge(n1.ID, 100500)
		require.Error(t, err, "It should not allow connection.")
	})
}

func TestGraph_CalculateSums(t *testing.T) {
	graph := NewGraph()

	// Setup

	//     [1: (1)]        sum=1
	//     /      \
	// [2: (2)]  [3: (2)]  sum=3 sum=3
	//     \      /
	//     [4: (3)]        sum=8
	expectations := []struct {
		id    int
		links []int
		val   int
		sum   int
	}{
		{id: 1, val: 1, links: []int{2, 3}, sum: 1},
		{id: 2, val: 2, links: []int{4}, sum: 3},
		{id: 3, val: 2, links: []int{4}, sum: 3},
		{id: 4, val: 3, links: []int{}, sum: 8},
	}

	for _, exp := range expectations {
		graph.AddNode(exp.id, exp.val)
	}
	for _, exp := range expectations {
		for _, link := range exp.links {
			graph.AddEdge(exp.id, link)
		}
	}

	// Subject
	graph.CalculateSums()

	// Assertions
	for _, exp := range expectations {
		assert.Equal(t, exp.sum, graph.GetNode(exp.id).Sum)
	}
}

func TestGraph_TSortEach(t *testing.T) {
	graph := NewGraph()

	// Setup

	//     1
	//   / |  \
	//  /  |   4
	// |   |  /
	// |   3
	//  \  |
	//   ` 2
	expectedSort := []int{1, 4, 3, 2}

	setups := []struct {
		id    int
		links []int
	}{
		{id: 1, links: []int{4, 3, 2}},
		{id: 2, links: []int{}},
		{id: 3, links: []int{2}},
		{id: 4, links: []int{3}},
	}
	for _, exp := range setups {
		graph.AddNode(exp.id, 0)
	}
	for _, exp := range setups {
		for _, link := range exp.links {
			graph.AddEdge(exp.id, link)
		}
	}

	// Subject
	sorted := make([]int, 0, len(setups))
	graph.TSortEach(func(node *Node, level int) {
		sorted = append(sorted, node.ID)
	})

	// Assetions
	assert.Equal(t, expectedSort, sorted, "It should iterate nodes in topological order.")
}
