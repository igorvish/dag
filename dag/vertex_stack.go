package dag

// Вспомогательные методы для работы со слайсом, как со стеком.

func newNodeStack() []*Node {
	return []*Node{}
}

func pushNodes(stack *[]*Node, node ...*Node) {
	*stack = append(*stack, node...)
}

func popNode(stack *[]*Node) *Node {
	l := len(*stack)

	if l == 0 {
		return nil
	}

	ret := (*stack)[l-1]
	*stack = (*stack)[:l-1]

	return ret
}

func peekNode(stack []*Node) *Node {
	l := len(stack)

	if l == 0 {
		return nil
	}

	return stack[l-1]
}
