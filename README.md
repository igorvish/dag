# Задача

> Дан граф в формате json. Рассчитать для узлов суммы так, чтобы они были равны суммам уникальных родителей.

# Реализация

В задаче реализована работа с ориентированным ацикличным графом.

Граф строится при помощи метдов `dag.AddNode()` и `dag.AddEdge()`.

Метод `dag.CalculateSums()` рассчитывает суммы для всех узлов, делая DFS-обход. Для узлов с единственным родителем суммы считаются по мере обхода, для узлов с несколькими родителями суммы считаются DFS-обходом родителей.

Метод `dag.TSortEach()` итератор с сортировкой узлов в топологическом порядке.

Структура:

```
├── dag
│   ├── graph.go         # Типы gag.Graph и dag.Node
│   ├── graph_test.go    # Юнит-тесты
│   └── vertex_stack.go  # Функции-хелеперы для работы со стеком
├── ...
└── main.go
```

В главном файле загружается структура из задания, подсчитываются суммы и печатается граф:

```bash
$ go run main.go 
# 1: val=2, sum=2 => [3 4 2]
#    4: val=4, sum=6 => [8]
#       8: val=2, sum=8 => []
#    2: val=3, sum=5 => [7 5]
#       7: val=2, sum=7 => []
#    3: val=3, sum=5 => [5]
#       5: val=2, sum=10 => [6]
#          6: val=2, sum=12 => []
```

Тесты:

```bash
$ go test -v ./...
# --- PASS: TestGraph_AddNode (0.00s)
# --- PASS: TestGraph_AddEdge (0.00s)
#     --- PASS: TestGraph_AddEdge/SuccessfulCase (0.00s)
#     --- PASS: TestGraph_AddEdge/When_specified_node_doesn't_esists (0.00s)
# --- PASS: TestGraph_CalculateSums (0.00s)
# --- PASS: TestGraph_TSortEach (0.00s)
# PASS
```